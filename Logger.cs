using Logging.Backends;
using System;
using System.Collections;
using System.Diagnostics;
using System.Reflection;
using System.Text;

namespace Logging
{
    /// <summary>
    /// Main class for logging
    /// </summary>
    public static class Logger
    {
        private static Backend defaultBackend;
        private static DateTime startTime;

        /// <summary>
        /// Gets or sets the default backend.
        /// </summary>
        /// <value>The default backend.</value>
        public static Backend DefaultBackend
        {
            get
            {
                if (defaultBackend == null) {
#if MICRO_FRAMEWORK
                    defaultBackend = new DebugBackend();
#else
                    defaultBackend = new ConsoleBackend();
#endif
                    defaultBackend.Enabled = true;
                    ResetTimer();
                }
                return defaultBackend;
            }
            set
            {
                defaultBackend = value;
            }
        }

        /// <summary>
        /// Gets or sets whether logging is enabled.
        /// </summary>
        public static bool Enabled
        {
            get
            {
                return DefaultBackend.Enabled;
            }
            set
            {
                DefaultBackend.Enabled = value;
            }
        }

        /// <summary>
        /// Resets the timer.
        /// </summary>
        public static void ResetTimer()
        {
            startTime = DateTime.Now;
        }

        /// <summary>
        /// Initializes the default backend: console and file.
        /// </summary>
        /// <param name="outputFile">Output file.</param>
        public static void InitializeDefaultBackendConsoleAndFile(string outputFile, bool enabled = true)
        {
            Backend[] backends = new Backend[] {
#if MICRO_FRAMEWORK
                new DebugBackend(),
#else
                new ConsoleBackend(),
#endif				
                new FileBackend(outputFile) {
                    IncludeTimestamp = true
                }
            };
            DefaultBackend = new MultipleBackend(backends);
            DefaultBackend.Enabled = enabled;
        }

        /// <summary>
        /// Logs the timer.
        /// </summary>
        public static void LogTimer()
        {
            LogTimer("");
        }

        /// <summary>
        /// Log the specified message.
        /// </summary>
        /// <param name="message">Message.</param>
        public static void Log(LogLevel level, string message)
        {
            DefaultBackend.Log(DateTime.Now, level, message);
        }

#if MICRO_FRAMEWORK
        /// <summary>
        /// Logs the timer.
        /// </summary>
        /// <param name="message">The message.</param>
        public static void LogTimer(string message)
        {
            TimeSpan difference = DateTime.Now - startTime;
            Log(LogLevel.Information, message + difference);
        }

        public static void Error(string message)
        {
            Log(LogLevel.Error, message);
        }

        public static void Information(string message)
        {
            Log(LogLevel.Information, message);
        }

        public static void Warning(string message)
        {
            Log(LogLevel.Warning, message);
        }

        public static void Debug(string message)
        {
            Log(LogLevel.Debug, message);
        }

        /// <summary>
        /// Log the specified exception with message.
        /// </summary>
        /// <param name="e">Exception.</param>
        /// <param name="message">Message.</param>
        public static void Log(Exception e, string message)
        {
            Error(message + " " + e.ToString());
        }

        /// <summary>
        /// Log the specified exception.
        /// </summary>
        /// <param name="e">Exception.</param>
        public static void Log(Exception e)
        {
            string message = "Stack trace: " + e.StackTrace;
            Log(e, message);
        }
#else
        /// <summary>
        /// Logs the timer.
        /// </summary>
        /// <param name="format">Format.</param>
        /// <param name="arguments">Arguments.</param>
        public static void LogTimer(string format, params object[] arguments)
        {
            TimeSpan difference = DateTime.Now - startTime;
            string message = Format(format, arguments);
            Log("{0}{1}", message, difference);
        }

        /// <summary>
        /// Log the message
        /// </summary>
        /// <param name="message">Message.</param>
        /// <param name="arguments">Arguments.</param>
        public static void Log(string message, params object[] arguments)
        {
            Log(LogLevel.Information, message, arguments);
        }

        /// <summary>
        /// Log the error.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <param name="arguments">Arguments.</param>
        public static void Error(string message, params object[] arguments)
        {
            Log(LogLevel.Error, message, arguments);
        }

        /// <summary>
        /// Log the information.
        /// </summary>
        /// <param name="message">Information message.</param>
        /// <param name="arguments">Arguments.</param>
        public static void Information(string message, params object[] arguments)
        {
            Log(LogLevel.Information, message, arguments);
        }

        /// <summary>
        /// Log the warning.
        /// </summary>
        /// <param name="message">Warning message.</param>
        /// <param name="arguments">Arguments.</param>
        public static void Warning(string message, params object[] arguments)
        {
            Log(LogLevel.Warning, message, arguments);
        }

        /// <summary>
        /// Log the debug message.
        /// </summary>
        /// <param name="message">Debug message.</param>
        /// <param name="arguments">Arguments.</param>
        public static void Debug(string message, params object[] arguments)
        {
            Log(LogLevel.Debug, message, arguments);
        }

        /// <summary>
        /// Log the specified message and arguments.
        /// </summary>
        /// <param name="message">Message.</param>
        /// <param name="arguments">Arguments.</param>
        public static void Log(LogLevel level, string message, params object[] arguments)
        {
            Log(DateTime.Now, level, message, arguments);
        }

        private static void Log(DateTime timeStamp, LogLevel level, string message, params object[] arguments)
        {
            DefaultBackend.Log(timeStamp, level, Format(message, arguments));
        }

        private static string Format(string format, params object[] arguments)
        {
            if (format == null) {
                return String.Empty;
            }
            return String.Format(format, arguments);
        }

        /// <summary>
        /// Log the specified exception with message and arguments.
        /// </summary>
        /// <param name="e">Exception.</param>
        /// <param name="message">Message.</param>
        /// <param name="arguments">Arguments.</param>
        public static void Log(Exception e, string message, params object[] arguments)
        {
            DateTime now = DateTime.Now;
            DefaultBackend.Log(now, LogLevel.Error, String.Format("{0} {1}", Format(message, arguments), e.ToString()));
            if (e.Data != null && e.Data.Count > 0) {
                DefaultBackend.Log(now, LogLevel.Error, "Additional Data:");
                foreach (DictionaryEntry data in e.Data) {
                    Log(now, LogLevel.Error, "\t{0}: {1}", data.Key, data.Value);
                }
            }
        }

        /// <summary>
        /// Log the specified exception.
        /// </summary>
        /// <param name="e">Exception.</param>
        public static void Log(Exception e)
        {
            StackTrace stackTrace = new StackTrace();
            MethodBase method = stackTrace.GetFrame(1).GetMethod();
            Log(e, String.Format("In {0}.{1}:", method.DeclaringType.Name, method.Name));
        }

        /// <summary>
        /// Log the specified stackTrace.
        /// </summary>
        /// <param name="stackTrace">Stack trace.</param>
        public static void Log(StackTrace stackTrace)
        {
            StackFrame[] stackFrames = stackTrace.GetFrames();
            foreach (StackFrame frame in stackFrames) {
                MethodBase method = frame.GetMethod();
                Log("\tat {0}{1}({2}) in {3}: line {4}, col {5}",
                     method.Name,
                     method.BuildGenericArgumentString(),
                     method.BuildParameterString(),
                     frame.GetFileName(),
                     frame.GetFileLineNumber(),
                     frame.GetFileColumnNumber());
            }
        }

        /// <summary>
        /// Logs the current time.
        /// </summary>
        public static void LogTime()
        {
            Log(String.Format("{0}", DateTime.Now));
        }

        private static string BuildGenericArgumentString(this MethodBase method)
        {
            if (!method.IsGenericMethod) {
                return String.Empty;
            }
            StringBuilder builder = new StringBuilder();
            Type[] types = method.GetGenericArguments();
            builder.Append("<");
            for (int i = 0; i < types.Length; i++) {
                Type type = types[i];
                if (i > 0) {
                    builder.Append(", ");
                }
                builder.Append(type.FullName);
            }
            builder.Append(">");
            return builder.ToString();
        }

        private static string BuildParameterString(this MethodBase method)
        {
            StringBuilder builder = new StringBuilder();
            ParameterInfo[] parameters = method.GetParameters();
            for (int i = 0; i < parameters.Length; i++) {
                ParameterInfo parameter = parameters[i];
                if (i > 0) {
                    builder.Append(", ");
                }
                builder.Append(parameter.ParameterType);
                builder.Append(" ");
                builder.Append(parameter.Name);
            }
            return builder.ToString();
        }
#endif
    }
}

