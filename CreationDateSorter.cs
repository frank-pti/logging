using System;
#if MICRO_FRAMEWORK
using Microsoft.SPOT;
#endif
using System.Collections;
using System.IO;

namespace Logging
{
    public static class CreationDateSorter
    {
        public static IEnumerable Sort(this IEnumerable e)
        {
            ICollection d = e as ICollection;
            object[] data = null;
            if (d == null) {
                ArrayList temp = new ArrayList();
                foreach (object o in e) {
                    temp.Add(o);
                }
                data = temp.ToArray();
                if (data.Length <= 1) {
                    return data;
                }
            } else {
                if (d.Count <= 1) {
                    return d;
                }
                data = new object[d.Count];
                d.CopyTo((object[])data, 0);
            }
            return MergeSort(data);
        }

        private static IEnumerable MergeSort(object[] data)
        {
            object[] left = SplitArray(data, 0, data.Length / 2 - 1);
            object[] right = SplitArray(data, data.Length / 2, data.Length - 1);

            if (left.Length > 1)
                MergeSort(left);
            if (right.Length > 1)
                MergeSort(right);

            Merge(left, right, data);

            return data;
        }

        private static void Merge(object[] left, object[] right, object[] result)
        {
            int i = 0, j = 0, h = 0;

            while (i < left.Length || j < right.Length) {
                if (i == left.Length)
                    result[h++] = right[j++];
                else if (j == right.Length)
                    result[h++] = left[i++];
                else if (CompareCreationDateTime(GetCreationTimeUtc(left[i]), GetCreationTimeUtc(right[j])) < 0) {
                    result[h++] = left[i++];
                } else {
                    result[h++] = right[j++];
                }
            }
        }

        private static object[] SplitArray(object[] array, int start, int end)
        {
            object[] result = new object[end - start + 1];
            Array.Copy(array, start, result, 0, result.Length);
            return result;
        }

        private static DateTime GetCreationTimeUtc(object o)
        {
            if (o is FileInfo) {
                return ((o as FileInfo).CreationTimeUtc);
            }
            return default(DateTime);
        }

        private static int CompareCreationDateTime(DateTime utc1, DateTime utc2)
        {
            return utc1.CompareTo(utc2);
        }

        public static int CompareCreationDateTime(FileInfo file1, FileInfo file2)
        {
            return CompareCreationDateTime(file1.CreationTimeUtc, file2.CreationTimeUtc);
        }
    }
}
