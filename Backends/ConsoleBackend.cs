#if !MICRO_FRAMEWORK
using System;

namespace Logging.Backends
{
    /// <summary>
    /// Logger backend for writing to the console (standard out).
    /// </summary>
    public class ConsoleBackend : Backend
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Logging.Backends.ConsoleBackend"/> class.
        /// </summary>
        public ConsoleBackend()
        {
        }

        /// <inheritdoc/>
        public override void Log(DateTime timeStamp, LogLevel level, string message)
        {
            if (Enabled) {
                Console.WriteLine(BuildLogLine(timeStamp, level, message));
            }
        }
    }
}
#endif
