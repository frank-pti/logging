#if MICRO_FRAMEWORK
using System;
using Microsoft.SPOT;

namespace Logging.Backends
{
    public class DebugBackend : Backend
    {
        public DebugBackend()
        {
        }

        public override void Log(DateTime timeStamp, LogLevel level, string message)
        {
            if (Enabled) {
                Debug.Print(BuildLogLine(timeStamp, level, message));
            }
        }
    }
}
#endif
