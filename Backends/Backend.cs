using System;
using System.ComponentModel;
using System.Reflection;

namespace Logging.Backends
{
    /// <summary>
    /// Base class for logger backends.
    /// </summary>
    public abstract class Backend
    {
        private const string TimeFormat = "yyyy-MM-dd hh:mm:ss.fffzzz";
#if MICRO_FRAMEWORK
        private static readonly string[] LogLevels = new string[] { "ERROR", "WARN ", "INFO ", "DEBUG" };
#endif

        private bool enabled;
        private bool includeTimeStamp;

        /// <summary>
        /// Log the specified message.
        /// </summary>
        /// <param name="message">Message.</param>
        public abstract void Log(DateTime timeStamp, LogLevel level, string message);

        /// <summary>
        /// Gets or sets whether logging should be enabled.
        /// </summary>
        public virtual bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
#if MICRO_FRAMEWORK
                string message = "Logging switched " + (enabled ? "on" : "off") + ".";
#else
                String message = String.Format("Logging switched {0}.", enabled ? "on" : "off");
#endif
                Log(DateTime.Now, LogLevel.Information, message);
                enabled = value;
            }
        }

        /// <summary>
        /// Gets or sets whether logging should include a time stamp.
        /// </summary>
        public virtual bool IncludeTimestamp
        {
            get
            {
                return includeTimeStamp;
            }
            set
            {
                includeTimeStamp = value;
            }
        }

        /// <summary>
        /// Build up a single text line for logging.
        /// </summary>
        /// <param name="timeStamp">The time stamp.</param>
        /// <param name="level">The log level.</param>
        /// <param name="message">The log message.</param>
        /// <returns>The log line.</returns>
        protected string BuildLogLine(DateTime timeStamp, LogLevel level, string message)
        {
            string line;
            string levelString = GetLogLevelDescription(level);
            if (IncludeTimestamp) {
#if MICRO_FRAMEWORK
                line = timeStamp.ToString(TimeFormat) + " " + levelString + " " + message;
#else
                line = String.Format("{0} {1} {2}", timeStamp.ToString(TimeFormat), levelString, message);
#endif
            } else {
#if MICRO_FRAMEWORK
                line = levelString + " " + message;
#else
                line = String.Format("{0} {1}", levelString, message);
#endif
            }
            return line;
        }

        protected static string GetLogLevelDescription(LogLevel level)
        {
#if MICRO_FRAMEWORK
            int index = (int)level;
            if (index < 0 || index > LogLevels.Length) {
                index = 0;
            }
            return LogLevels[index];
#else
            FieldInfo field = level.GetType().GetField(level.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes != null && attributes.Length > 0) {
                return attributes[0].Description;
            }
            return level.ToString();
#endif
        }
    }
}
