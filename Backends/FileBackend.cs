using System;
using System.Collections;
using System.IO;

namespace Logging.Backends
{
    /// <summary>
    /// Logger backend for logging to a file.
    /// </summary>
    public class FileBackend : Backend
    {
        private readonly string FileNameBase;
        private string fileName;
        private long maxFileSize;
        private int maxFileCount;
        private FileStream fileStream;
        private TextWriter textWriter;
#if !MICRO_FRAMEWORK
        private FileSystemWatcher fileSizeWatcher;
#endif

        /// <summary>
        /// Initializes a new instance of the <see cref="Logging.Backends.FileBackend"/> class.
        /// </summary>
        /// <param name="fileName">File name.</param>
        /// <remarks>There will be no limit for file size and amount of files.</remarks>
        public FileBackend(string fileName) :
            this(fileName, 0, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logging.Backends.FileBackend"/> class.
        /// </summary>
        /// <param name="fileName">File name.</param>
        /// <param name="maxFileSize">Maximum file size in Byte.</param>
        /// <param name="maxFileCount">Maximum count of files (only if maximum file size is set).</param>
        public FileBackend(string fileName, long maxFileSize, int maxFileCount)
        {
            FileNameBase = ExtractFileNameBase(fileName);
            this.fileName = fileName;
            this.maxFileSize = maxFileSize;
            this.maxFileCount = maxFileCount;
            InitializeFileSizeWatcher(fileName);
        }

        private void InitializeFileSizeWatcher(string fileName)
        {
            if (maxFileSize > 0) {
#if MICRO_FRAMEWORK
                Log(LogLevel.Warning, "Cannot initialize file watching under .NET MF! Large files won't be split!");
#else
                FileInfo file = new FileInfo(fileName);
                fileSizeWatcher = new FileSystemWatcher(file.DirectoryName, file.Name);
                fileSizeWatcher.Changed += FileSizeWatcherChanged;
                fileSizeWatcher.EnableRaisingEvents = true;
#endif
            }
        }

        private static string ExtractFileNameBase(string fileName)
        {
            int directorySepartorIndex = fileName.LastIndexOf(Path.DirectorySeparatorChar);
            int extensionSeparatorIndex = fileName.LastIndexOf(".");
            return fileName.Substring(directorySepartorIndex + 1, extensionSeparatorIndex - directorySepartorIndex - 1);
        }

        private static string ExtractFileExtension(string fileName)
        {
            return fileName.Substring(fileName.LastIndexOf(".") + 1);
        }

        private static string BuildFileName(string directory, string name)
        {
            return Path.Combine(directory, name);
        }

#if !MICRO_FRAMEWORK
        private void FileSizeWatcherChanged(object sender, FileSystemEventArgs e)
        {
            switch (e.ChangeType) {
                case WatcherChangeTypes.Deleted:
                    fileSizeWatcher.Changed -= FileSizeWatcherChanged;
                    break;
                case WatcherChangeTypes.Changed:
                    FileInfo file = new FileInfo(fileName);
                    if (file.Length >= maxFileSize) {
                        lock (this) {
                            fileSizeWatcher.EnableRaisingEvents = false;
                            fileSizeWatcher.Changed -= FileSizeWatcherChanged;
                            string oldName = fileName;
                            string extension = ExtractFileExtension(fileName);
                            string newName = null;
                            int count = 1;
                            do {
                                newName = String.Format("{0} ({1}).{2}", FileNameBase, count, extension);
                                count++;
                            } while (File.Exists(BuildFileName(file.DirectoryName, newName)));
                            Log(LogLevel.Information, String.Format("Log will be continued in: '{0}'", newName));
                            textWriter.Flush();
                            textWriter.Close();
                            textWriter = null;
                            fileStream.Close();
                            fileStream = null;
                            fileName = BuildFileName(file.DirectoryName, newName);
                            Log(LogLevel.Information, String.Format("Log is continued from: '{0}'", oldName));
                            InitializeFileSizeWatcher(fileName);
                            if (maxFileCount > 0) {
                                CleanUpLogFiles(file.Directory, maxFileCount, String.Format("{0}*.log", FileNameBase));
                            }
                        }
                    }
                    break;
                default:
                    Logger.Log("FileBackend noticed '{0}' file event", e.ChangeType.ToString());
                    break;
            }
        }
#endif

        private TextWriter TextWriter
        {
            get
            {
                if (fileStream == null) {
                    FileInfo file = new FileInfo(fileName);
                    if (!file.Directory.Exists) {
                        Directory.CreateDirectory(file.DirectoryName);
                    }
                    fileStream = File.Open(fileName, FileMode.CreateNew, FileAccess.Write, FileShare.ReadWrite);
                    textWriter = new StreamWriter(fileStream);
                }
                return textWriter;
            }
        }

        private void Log(LogLevel level, string message)
        {
            Log(DateTime.Now, level, message);
        }

        /// <inheritdoc/>
        public override void Log(DateTime timeStamp, LogLevel level, string message)
        {
            if (!Enabled) {
                return;
            }
            TextWriter.WriteLine(BuildLogLine(timeStamp, level, message));
            TextWriter.Flush();
        }

        /// <summary>
        /// Remove older log files in order to reduce log file count to a certain value in specified directory.
        /// </summary>
        /// <param name="logDirectory">The directory where the clean up should take place.</param>
        /// <param name="maxLogFileCount">The amount of log files to keep.</param>
        public static void CleanUpLogFiles(string logDirectory, int maxLogFileCount)
        {
            CleanUpLogFiles(new DirectoryInfo(logDirectory), maxLogFileCount);
        }

        /// <summary>
        /// Remove older log files in order to reduce log file count to a certain value in specified directory.
        /// </summary>
        /// <param name="logDirectory">The directory where the clean up should take place.</param>
        /// <param name="maxLogFileCount">The amount of log files to keep.</param>
        public static void CleanUpLogFiles(DirectoryInfo logDirectory, int maxLogFileCount)
        {
            CleanUpLogFiles(logDirectory, maxLogFileCount, "*.log");
        }

        /// <summary>
        /// Remove older log files in order to reduce log file count to a certain value in specified directory. The
        /// algorithm will use the specified pattern for file filtering.
        /// </summary>
        /// <param name="logDirectory">The directory where the clean up should take place.</param>
        /// <param name="maxLogFileCount">The amount of log files to keep.</param>
        /// <param name="pattern">The file name pattern.</param>
        public static void CleanUpLogFiles(DirectoryInfo logDirectory, int maxLogFileCount, string pattern)
        {
            try {
#if MICRO_FRAMEWORK
                FileInfo[] allFiles = logDirectory.GetFiles();
                ArrayList logFileList = new ArrayList();
                foreach (FileInfo file in allFiles) {
                    int length = file.FullName.Length;
                    if (file.FullName.IndexOf(pattern, length - pattern.Length) != -1) {
                        logFileList.Add(file);
                    }
                }
                FileInfo[] logFiles = (FileInfo[])logFileList.ToArray();
                logFiles.Sort();
#else
                FileInfo[] logFiles = logDirectory.GetFiles(pattern);
                Array.Sort(logFiles, CreationDateSorter.CompareCreationDateTime);
#endif
                int deleteCount = Math.Max(logFiles.Length - maxLogFileCount, 0);
                for (int i = 0; i < deleteCount; i++) {
                    try {
                        logFiles[i].Delete();
                    } catch (Exception e) {
                        Logger.Log(e);
                    }
                }
#if MICRO_FRAMEWORK
                Logger.Log(LogLevel.Information, "Deleted " + deleteCount + " old log files");
#else
                Logger.Log("Deleted {0} old log files", deleteCount);
#endif
            } catch (Exception e) {
                Logger.Log(e);
            }
        }
    }
}

