
using System;
namespace Logging.Backends
{
    /// <summary>
    /// Logger backend for multiple backends (e.g. console and file backend)
    /// </summary>
    public class MultipleBackend : Backend
    {
        private Backend[] backends;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logging.Backends.MultipleBackend"/> class.
        /// </summary>
        /// <param name="backends">Array of Backends.</param>
        public MultipleBackend(Backend[] backends)
        {
            this.backends = backends;
        }

        /// <inheritdoc/>
        public override void Log(DateTime timeStamp, LogLevel level, string message)
        {
            foreach (Backend backend in backends) {
                backend.Log(timeStamp, level, message);
            }
        }

        /// <inheritdoc/>
        public override bool Enabled
        {
            get
            {
                return base.Enabled;
            }
            set
            {
                base.Enabled = value;
                foreach (Backend backend in backends) {
                    backend.Enabled = value;
                }
            }
        }

        /// <inheritdoc/>
        public override bool IncludeTimestamp
        {
            get
            {
                return base.IncludeTimestamp;
            }
            set
            {
                base.IncludeTimestamp = value;
                foreach (Backend backend in backends) {
                    backend.Enabled = value;
                }
            }
        }

        /// <summary>
        /// Add a new backend
        /// </summary>
        /// <param name="backend">The backend to be added.</param>
        public void Add(Backend backend)
        {
            Backend[] newArray = new Backend[backends.Length + 1];
            backends.CopyTo(newArray, 0);
            newArray[newArray.Length - 1] = backend;
            backends = newArray;
        }
    }
}

