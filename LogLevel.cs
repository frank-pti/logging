﻿#if !MICRO_FRAMEWORK
using System.ComponentModel;
#endif

namespace Logging
{
    /// <summary>
    /// Log level
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// Execution of some task could not be completed.
        /// </summary>
#if !MICRO_FRAMEWORK
        [Description("ERROR")]
#endif
        Error,

        /// <summary>
        /// Something unexpected happened, but execution can continue, e.g. using default values
        /// </summary>
#if !MICRO_FRAMEWORK
        [Description("WARN ")]
#endif
        Warning,

        /// <summary>
        /// Something normal but significant happened.
        /// </summary>
#if !MICRO_FRAMEWORK
        [Description("INFO ")]
#endif
        Information,

        /// <summary>
        /// Something normal and insignificant happened.
        /// </summary>
#if !MICRO_FRAMEWORK
        [Description("DEBUG")]
#endif
        Debug
    }
}
